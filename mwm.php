<?php

/*{{{ Front */

/*{{{ Charge les scripts front */
add_action( 'wp_enqueue_scripts', 'mwm_front_enqueue_scripts' );
function mwm_front_enqueue_scripts(){
  wp_enqueue_script(
    'front',
    get_stylesheet_directory_uri() . '/js/front.js',
    array( 'jquery' )
  );
}
/*}}}*/

/*{{{ Charge les traductions */
add_action( 'after_setup_theme', 'mwm_load_child_theme_textdomain' );
function mwm_load_child_theme_textdomain() {
  load_child_theme_textdomain(
    'coordination_sud',
    get_stylesheet_directory() . '/lang'
  );
}
/*}}}*/

/*{{{ Charge les polices */
add_action( 'wp_head', 'mwm_load_fonts', 0 );
function mwm_load_fonts() {
  echo '<link rel = "preconnect" href = "https://use.typekit.net" crossorigin >';
  echo '<link rel = "stylesheet" href = "https://use.typekit.net/psl3ggy.css" >';
}
/*}}}*/

/*{{{ Ajoute les classes CSS supplémentaires au body */
add_filter( 'body_class', 'mwm_add_body_classes' );
function mwm_add_body_classes( $classes ) {
  $extra_classes = array();
  $extra_classes[] = '';
  return array_merge( $classes, $extra_classes );
}
/*}}}*/

/*{{{ Supprime les éléménts Wordpress par défaut */

/* Version de Wordpress */
function mwm_remove_wp_version() {
  return '';
}
add_filter( 'the_generator', 'mwm_remove_wp_version' );

/* Meta générateur */
remove_action( 'wp_head', 'wp_generator' );

/* Lien Really Simple Discovery */
remove_action( 'wp_head', 'rsd_link' );

/* Windows Live Writer */
remove_action( 'wp_head', 'wlwmanifest_link' );

/* Flux RSS */
remove_action( 'wp_head', 'feed_links', 2 );

/* Flux RSS des commentaires */
remove_action( 'wp_head', 'feed_links_extra', 3 );

/*}}}*/

/*{{{ Désactive les flux RSS pour certains types d'articles */
add_action( 'do_feed', 'mwm_disable_rss_feeds', 1 );
add_action( 'do_feed_rdf', 'mwm_disable_rss_feeds', 1 );
add_action( 'do_feed_rss', 'mwm_disable_rss_feeds', 1 );
add_action( 'do_feed_rss2', 'mwm_disable_rss_feeds', 1 );
add_action( 'do_feed_atom', 'mwm_disable_rss_feeds', 1 );
add_action( 'do_feed_rss2_comments', 'mwm_disable_rss_feeds', 1 );
add_action( 'do_feed_atom_comments', 'mwm_disable_rss_feeds', 1 );
function mwm_disable_rss_feeds() {
  global $post;
  if ( $post -> post_type == "p_annonces" ) {
    wp_die(
      __( 'No feed available, please visit our <a href="'. get_bloginfo('url') .'">homepage</a>!', 'coordination_sud' )
    );
  }
}
/*}}}*/

/*{{{ Modifie la requète pour les pages de résultats de recherche libre */
add_filter( 'pre_get_posts', 'mwm_modify_free_search_results_query' );
function mwm_modify_free_search_results_query( $query ) {
  if (
    !is_admin() &&
    $query -> is_search() &&
    $query -> is_main_query()
  ) {
    $query -> set(
      'post_type',
      array(
        'page',
        'xxx'
      )
    );
    $query -> set( 'post_password', '' );
    $query -> set( 'post_status', 'publish' );
    $query -> set( 'orderby', 'date' );
    $query -> set( 'order', 'DESC' );
  }
  return $query;
}
/*}}}*/

/*{{{ Modifie la requète pour les pages d'archive de termes */
add_filter( 'pre_get_posts', 'mwm_modify_archive_query' );
function mwm_modify_archive_query( $query ) {
  if(
    !is_admin() &&
    $query -> is_archive() &&
    $query -> is_main_query()
  ) {
    $query -> set(
      'orderby', array( 'type' => 'ASC', 'date' => 'DESC' )
    );
  }
}
/*}}}*/

/*{{{ Modifie la requète pour les pages d'archive d'auteurs */
add_filter( 'pre_get_posts', 'mwm_modify_author_archive_query' );
function mwm_modify_author_archive_query( $query ) {
  if (
    !is_admin() &&
    $query -> is_author()
    && $query -> is_main_query()
  ) {
    /*{{{ Nombre de résultats */
    $query -> set( 'posts_per_page', -1 );
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    $query -> set( 'paged', $paged );
    /*}}}*/
    /*{{{ Types d'articles */
    $query -> set(
      'post_type',
      array(
        'page',
        'xxx',
      )
    );
    /*}}}*/
    /*{{{ Classe par type d'articles */
    $query -> set( 'orderby', array( 'type' => 'ASC', 'date' => 'DESC' ) );
    /*}}}*/
    return $query;
  }
}
/*}}}*/

/*{{{ Enfold: Modifie la requète de la recherche libre AJAX */
if ( !function_exists( 'mwm_enfold_filter_ajax_search_results' ) ) {
  add_filter( 'avf_ajax_search_query', 'mwm_enfold_filter_ajax_search_results', 10, 1 );
  function mwm_enfold_filter_ajax_search_results( $search_parameters ) {
    $defaults = array(
      'posts_per_page' => 8,
      'post_type' => array(
        'page',
        'xxx'
      ),
      'post_status' => 'publish',
      'post_password' => '',
      'orderby' => 'date',
      'order' => 'DESC'
    );
    $_REQUEST['s'] = apply_filters( 'get_search_query', $_REQUEST['s'] );
    $search_parameters = array_merge( $defaults, $_REQUEST );
    return $search_parameters;
  }
}
/*}}}*/

/*{{{ ACF : Modifie les textes placeholder pour les champs taxonomy */
add_filter( 'acf/load_field/type=taxonomy', 'mwm_acf_load_field_taxonomy' );
function mwm_acf_load_field_taxonomy( $field ) {
  $field['placeholder'] = ' ';
  switch ( $field['name'] ) {
  case "xxx" :
    $field['placeholder'] = __( 'xxx', 'xxx' );
    break;
  case "xxx" :
    $field['placeholder'] = __( 'xxx', 'xxx' );
    break;
  }
  return $field;
}
/*}}}*/

/*{{{ ACF : Peuple le champ déroulant « xxx » */
add_filter( 'acf/load_field/name=xxx', 'mwm_acf_populate_xxx' );
function mwm_acf_populate_xxx( $field ) {
  $field['choices'] = array();
  $args = array( 'role' => 'xxx' );
  $query = new WP_User_Query( $args );
  $users = $query -> get_results();
  if ( $users ) {
    $field['choices'][ 'default' ] = 'xxx';
    foreach ( $users as $user ) {
      $args = array(
        'author' => $user -> ID,
        'post_type' => 'xxx',
        'posts_per_page' => 1
      );
      $query = new WP_Query( $args );
      if ( $query -> have_posts() ) {
        while ( $query -> have_posts() ) {
          $query -> the_post();
          $field['choices'][ sanitize_title( $user -> user_login ) ] =  $query -> post -> post_title;
        }
        wp_reset_postdata();
      }
    }
  }
  natcasesort( $field['choices'] );
  return $field;
}
/*}}}*/

/*{{{ ACF : Définit le message de validation du champ « xxx » */
add_filter( 'acf/validate_value/name=xxx', 'mwm_acf_validate_xxx', 10, 4 );
function mwm_acf_validate_xxx( $valid, $value, $field, $input ) {
  if ( !$valid ) {
    $valid = __( 'xxx', 'xxx' );
    return $valid;
  }
  return $valid;
}
/*}}}*/

/*{{{ ACF : Limite le nombre de choix pour les « xxx » */
add_filter( 'acf/validate_value/name=xxx', 'mwm_acf_validate_xxx', 10, 4 );
function mwm_acf_validate_xxx( $valid, $value, $field, $input ) {
  if ( !$valid ) {
    return $valid;
  }
  if ( sizeof( $value ) > x ) {
    $valid = __( 'Please select no more than x terms', 'xxx' );
  } else {
    $valid = true;
  }
  return $valid;
}
/*}}}*/

/*{{{ FacetWP : Crée les facets */
add_filter( 'facetwp_facets', 'mwm_fwp_register_facets' );
function mwm_fwp_register_facets( $facets ) {
  /*{{{ Recherche libre */
  $facets[] = array(
    'label' => 'Recherche libre',
    'name' => 'recherche_libre',
    'type' => 'search',
    'search_engine' => 'searchwp',
    'placeholder' => __( 'Free search', 'xxx' ),
  );
  /*}}}*/
  /*{{{ Thèmes */
  $facets[] = array(
    'label' => 'Thèmes',
    'name' => 'themes',
    'type' => 'checkboxes',
    'source' => 'tax/themes',
    'parent_term' => '',
    'hierarchical' => 'no',
    'orderby' => 'display_value',
    'show_expanded' => 'no',
    'operator' => 'or',
    'soft_limit' => '8',
    'ghosts' => 'yes',
    'preserve_ghosts' => 'no',
    'count' => '100'
  );
  /*}}}*/
  /*{{{ Nombre de résultats */
  $facets[] = array(
    'label' => 'Nombre de résultats',
    'name' => 'nb_resultats',
    'type' => 'pager',
    'pager_type' => 'counts',
    'count_text_plural' => '<strong>[total] ' . __( 'results', 'xxx' ) . '</strong> ' . __( 'for your search', 'xxx' ),
    'count_text_singular' => '<strong>1 ' . __( 'result', 'xxx' ) . '</strong> ' . __( 'for your search', 'xxx' ),
  );
  /*}}}*/
  return $facets;
};
/*}}}*/

/*{{{ FacetWP : Crée les templates */
add_filter( 'facetwp_templates', 'mwm_fwp_register_templates' );
function mwm_fwp_register_templates( $templates ) {
  /*{{{ xxx */
  $templates[] = array(
    'name' => 'xxx',
    'label' => 'xxx',
    'template' => '<?php include( get_stylesheet_directory() . "/includes/loop-posts.php" ); ?>',
    'query_obj' => array(
      'post_type' => array(
        array(
          'label' => 'xxx',
          'value' => 'xxx',
        ),
      ),
      'posts_per_page' => 10,
      'post_status' => 'publish'
    ),
    'modes' => array(
      'display' => 'advanced',
      'query' => 'visual',
    ),
  );
  /*}}}*/
  return $templates;
}
/*}}}*/

/*{{{ FacetWP : Modifie la requète */
add_filter( 'facetwp_query_args', 'mwm_fwp_alter_query', 10, 2 );
function mwm_fwp_alter_query( $query_args, $class ) {
  $uri = $class -> ajax_params['http_params']['uri'];
  /*{{{ xxx */
  if ( 'xxx' == $class -> ajax_params['template'] ) {
    if ( $_GET['_types_actualite'] == 'p_evenements' ) {
      /*{{{ Affiche uniquement les événements futurs */
      $today = date('Ymd');
      $query_args['meta_query'] = array(
        array(
          'key' => 'date-fin',
          'value' => $today,
          'type' => 'NUMERIC',
          'compare' => '>='
        )
      );
      /*}}}*/
      /*{{{ Classe les événements par date de validité */
      $query_args['meta_key'] = 'date-fin';
      $query_args['orderby'] = 'meta_value';
      /*}}}*/
    }
  }
  /*}}}*/
  return $query_args;
};
/*}}}*/

/*{{{ FacetWP : Crée les choix sur mesure du facet « xxx » */
add_filter( 'facetwp_facet_render_args', 'mwm_fwp_custom_xxx_choices' );
function mwm_fwp_custom_xxx_choices( $args ) {
  if ( 'xxx' == $args['facet']['name'] ) {
    $args['values'][] = array(
      'facet_value' => 'xxx',
      'facet_display_value' => 'xxx',
    );
  }
  return $args;
};
/*}}}*/

/*{{{ FacetWP : Modifie le texte du lien « voir plus » */
add_filter( 'gettext', 'mwm_fwp_alter_texts', 10, 3 );
function mwm_fwp_alter_texts( $translated_text, $text, $domain ) {
  if ( 'fwp-front' == $domain && 'Voir {num} plus' == $translated_text ) {
    $translated_text = 'Voir plus';
  }
  return $translated_text;
}
/*}}}*/

/*}}}*/

/*{{{ Backoffice */

/*{{{ Charge les styles du backoffice (global) */
add_action( 'admin_enqueue_scripts', 'mwm_load_admin_styles' );
function mwm_load_admin_styles(){
  wp_enqueue_style(
    'xxx_admin',
    get_stylesheet_directory_uri() . '/css/admin.css'
  );
}
/*}}}*/

/*{{{ Charge les styles du backoffice (éditeur) */
add_editor_style( 'css/editor.css' );
/*}}}*/

/*{{{ Charge les scripts du backoffice */
add_action( 'admin_enqueue_scripts', 'mwm_admin_enqueue_scripts' );
function mwm_admin_enqueue_scripts() {
  wp_register_script(
    'xxx_admin',
    get_stylesheet_directory_uri() . '/js/admin.js',
    array( 'jquery' )
  );
  /*{{{ Traductions */
  $translations = array(
    'mandatory_title' => __( 'Please enter a title', 'xxx' )
  );
  wp_localize_script( 'xxx_admin', 'translations', $translations );
  wp_enqueue_script( 'xxx_admin' );
  /*}}}*/
}
/*}}}*/

/*{{{ Ajoute des classes supplémentaires au body dans le backoffice */
add_filter( 'admin_body_class', 'mwm_add_admin_body_classes' );
function mwm_add_admin_body_classes( $classes ) {
  $classes .= ' xxx ';
  return $classes;
}
/*}}}*/

/*{{{ Supprime les champs profils réseaux sociaux du profil */
add_filter( 'user_contactmethods', 'mwm_remove_contact_methods' );
function mwm_remove_contact_methods( $contactmethods ) {
  unset( $contactmethods['facebook'] );
  unset( $contactmethods['instagram'] );
  unset( $contactmethods['myspace'] );
  unset( $contactmethods['pinterest'] );
  unset( $contactmethods['soundcloud'] );
  unset( $contactmethods['tumblr'] );
  unset( $contactmethods['twitter'] );
  unset( $contactmethods['linkedin'] );
  unset( $contactmethods['wikipedia'] );
  unset( $contactmethods['youtube'] );
  return $contactmethods;
}
/*}}}*/

/*{{{ Règle les rôles à afficher dans le déroulant auteur */
add_action( 'wp_dropdown_users_args', 'mwm_add_roles_to_authors_dropdown' );
function mwm_add_roles_to_authors_dropdown( $args ) {
  if ( isset( $args['who'] ) ) {
    $args['role__in'] = array(
      'administrator',
      'xxx'
    );
    unset( $args['who'] );
  }
  return $args;
}
/*}}}*/

/*{{{ Classe les rôles par ordre alphabétique */
add_filter( 'editable_roles', 'mwm_sort_user_roles' );
function mwm_sort_user_roles( $roles ){
  uasort( $roles, 'mwm_uasort_roles' );
  return $roles;
}
function mwm_uasort_roles( $a, $b ){
  return strcasecmp(
    translate_user_role( $b['name'] ),
    translate_user_role( $a['name'] )
  );
}
/*}}}*/

/*{{{ Définit les formats d'image. */

/* WP : Miniature (thumbnail) */
add_image_size( 'thumbnail', 250, 150, true );

/* WP : Moyenne (medium) */
add_image_size( 'medium', 300, 180, true );

/* WP : Grande (large) */
add_image_size( 'large', 400, 240, true );

/* CUSTOM : Entête page/article pleine largeur */
add_image_size( 'post_header', 660, 396, true );

/* CUSTOM : Portrait miniature */
add_image_size( 'portrait_thumbnail', 150, 215, array( 'center', 'top' ) );

/*}}}*/

/*{{{ Configure la qualité des images jpeg */
add_filter( 'jpeg_quality', 'mwm_register_image_quality' );
function mwm_register_image_quality() {
  return 80;
}
/*}}}*/

/*{{{ Configure les révisions d'articles */
define( 'WP_POST_REVISIONS', false );
/*}}}*/

/*{{{ Affiche le bouton « Obtenir le lien court » après le permalien */
add_filter( 'get_shortlink', 'mwm_get_shortlink' );
function mwm_get_shortlink( $shortlink ){
  return $shortlink;
}
/*}}}*/

/*{{{ Modifie le texte par défaut du champ « Titre » */
add_filter( 'enter_title_here', 'mwm_custom_enter_title_here' );
function mwm_custom_enter_title_here( $input ) {
  $input = __( 'xxx', 'xxx' );
  return $input;
}
/*}}}*/

/*{{{ Permet aux éditeurs de modifier les menus */
$role = get_role( 'editor' );
if ( !$role -> has_cap( 'edit_theme_options' ) ) {
  $role -> add_cap( 'edit_theme_options' );
}
/*}}}*/

/*{{{ TinyMCE : Déclare la barre d'outils et ajoute les styles sur mesure */
add_filter( 'tiny_mce_before_init', 'mwm_register_tinymce_buttons' );
function mwm_register_tinymce_buttons( $init ) {
  $screen = get_current_screen();
  /*{{{ Palette de couleurs */
  $default_colors = '
  "ffffff", "Blanc",
  ';
  $custom_colors = '';
  $init['textcolor_map'] = '[' . $default_colors . ',' . $custom_colors . ']';
  $init['textcolor_rows'] = 5;
  /*}}}*/
  /*{{{ Formats de base */
  $init['block_formats'] = "Paragraph=p;Heading 1=h2;Heading 2=h3;Heading 3=h4;Heading 4=h5;Heading 5=h6;";
  /*}}}*/
  /*{{{ Formats custom */
  $style_formats = array(
    array(
      'title' => 'Bloc xxx',
      'block' => 'div',
      'classes' => 'xxx',
      'wrapper' => true,
    ),
  );
  $init['style_formats'] = json_encode( $style_formats );
  /*}}}*/
  /*{{{ Barre d'outils */
  $init['toolbar1'] = 'formatselect,styleselect,forecolor,|,bold,italic,underline,|,alignleft,aligncenter,alignright,|,bullist,numlist,|,hr,charmap,blockquote,superscript,|,pastetext,removeformat,|,link,unlink,|,undo,redo';
  $init['toolbar2'] = '';
  /*}}}*/
  return $init;
}
/*}}}*/

/*{{{ TinyMCE : Supprime le déroulant de styles */
add_filter( 'mce_buttons_2', 'mwm_register_mce_buttons_2' );
function mwm_register_mce_buttons_2( $buttons ) {
  array_unshift( $buttons, 'styleselect' );
  return $buttons;
}
/*}}}*/

/*{{{ TinyMCE : Supprime les classes d'images superflues */
add_filter( 'get_image_tag_class', 'mwm_remove_image_classes' );
function mwm_remove_image_classes( $classes ){
  // Removes wp-image-[ID]
  $classes = preg_replace( '/ wp-image-([0-9])+/', "", $classes );
  // Remove size-[size]
  $classes = preg_replace( '/ size-([a-z])+/', "", $classes );
  return $classes;
}
/*}}}*/

/*{{{ TinyMCE : Supprime les images et vidéos dans les xxx */
add_filter( 'the_content', 'mwm_tinymce_remove_xxx' );
function mwm_tinymce_remove_xxx( $content ) {
  if ( get_post_type() == 'xxx' ) {
    $content =
      preg_replace(
        array('{<a(.*?)(wp-att|wp-content\/uploads)[^>]*><img}',
        '{ wp-image-[0-9]*" /></a>}'),
        array('<img','" />'),
        $content
      );
    $content = preg_replace("/<img[^>]+\>|<video[^>]+\>/i", " ", $content);
  }
  return $content;
}
/*}}}*/

/*{{{ Crée le shortcode « xxx » */
add_shortcode( 'xxx', 'mwm_shortcode_xxx' );
function mwm_shortcode_xxx( $atts ) {
  extract( shortcode_atts( array(
    'attr1' => 'xxx',
    'attr2' => 'xxx',
  ), $atts, 'xxx' ) );
  $output = '<' . $attr1 . ' class = "'. $attr2 . '" >xxx</' . $attr1 . '>';
  return $output;
}
/*}}}*/

/*{{{ ACF: Limite le nombre de choix pour un champ taxo */
add_filter( 'acf/validate_value/name=xxx', 'mwm_acf_validate_xxx', 10, 4 );
function mwm_acf_validate_xxx( $valid, $value, $field, $input ) {
  if ( !$valid ) {
    return $valid;
  }
  if ( sizeof( $value ) > 3 ) {
    $valid = __( 'Please select no more than 3 terms', 'xxx' );
  } else {
    $valid = true;
  }
  return $valid;
}
/*}}}*/

/*{{{ ACF : Limite le nombre de caractères maxi autorisé pour certains champs */
add_filter( 'acf/validate_value/name=xxx', 'mwm_acf_validate_xxx', 10, 4 );
function mwm_acf_validate_description( $valid, $value, $field, $input ) {
  if ( !$valid ) {
    return $valid;
  }
  $limit = xxx;
  $val = strlen( $value ) - 2;
  if ( $val > $limit ) {
    $valid = 'Merci d’entrer ' . $limit . ' caractères maximum, espaces compris.<br/>Vous avez entré ' . $val . ' caractères.';
  }
  return $valid;
}
/* }}} */

/*{{{ ACF : Rend obligatoire la saisie d'une date future limitée à x mois */
add_filter( 'acf/validate_value/name=xxx', 'mwm_acf_validate_xxx', 50, 4 );
function mwm_acf_validate_xxx( $valid, $value, $field, $input ) {
  if ( !$valid ) {
    return $valid;
  }
  $today = new DateTime( 'now' );
  if ( $value <= $today -> format( 'Ymd' ) ) {
    $valid = __( 'Please enter a future date', 'xxx' );
  }
  $limit = $today -> modify( '+x month' );
  if ( $value > $limit -> format( 'Ymd' ) ) {
    $valid = __( 'Cannot be valid for more than x months.', 'xxx' );
  }
  return $valid;
}
/*}}}*/

/*{{{ ACF : Autorise uniquement certains rôles à ajouter des termes dans l'UI ACF */
add_filter( 'acf/load_field/type=taxonomy', 'mwm_acf_restrict_add_term' );
function mwm_acf_restrict_add_term( $field ) {
  $user = wp_get_current_user();
  if ( !in_array( 'xxx', $user -> roles ) ) {
    $field['add_term'] = 0;
  }
  return $field;
}
/*}}}*/

/*{{{ ACF : Affiche les spécification attendues sur les champs image */
add_filter( 'acf/prepare_field/name=xxx', 'mwm_acf_display_image_specs' );
function mwm_acf_display_image_specs( $field ) {
  if ( $field['value'] ) {
    $field['instructions'] .= '<div class = "specs">';
    $field['instructions'] .= '<em title = "' . __( 'Your image must be at last this big', 'xxx' ) . '" >' . __( 'Min size', 'xxx' ) . '</em> : ' . $field['min_width'] . 'x' . $field['min_height'] . ' px <span class = "sep" >|</span><em title = "' . __( 'Your image must be of this format', 'xxx' ) . '">' . __( 'Format', 'xxx' ) . '</em> : ' . $field['mime_types'] . '<span class = "sep" >|</span><em title = "' . __( 'Your image must weight no more than this', 'xxx' ) . '">' . __( 'Max weight', 'xxx' ) . '</em> : ' . $field['max_size'] . 'Mb';
    $field['instructions'] .= '</div>';
  }
  return $field;
}
/*}}}*/

/*{{{ ACF : Règle le format du compteur de caractères */
add_filter( 'acf-input-counter/display', 'mwm_acf_counter_filter' );
function mwm_acf_counter_filter( $display ) {
  $display = sprintf(
    __( '%1$s / %2$s', 'acf-counter' ),
    '%%len%%',
    '%%max%%'
  );
  return $display;
}
/*}}}*/

/*{{{ ACF : Crée une page d'options */
if ( function_exists( 'acf_add_options_page' ) ) {
  acf_add_options_page(
    array(
      'page_title' => 'xxx',
      'menu_title' => 'xxx',
      'menu_slug'	=> 'xxx-settings',
      'capability' => 'edit_posts',
      'redirect' => false
    )
  );
}
/*}}}*/

/*{{{ ACF : Affiche uniquement les contenus publiés dans le type de champ « Post object » */
add_filter( 'acf/fields/post_object/query', 'mwm_acf_post_object_query', 10, 3 );
function mwm_acf_post_object_query( $args, $field, $post_id ) {
  $args['post_status'] = 'publish';
  return $args;
}
/* }}} */

/*{{{ ACF : Affiche uniquement les contenus publiés dans le type de champ « Relationship » */
add_filter( 'acf/fields/relationship/query', 'mwm_acf_post_object_query', 10, 3 );
function mwm_acf_relationship_query( $args, $field, $post_id ) {
  $args['post_status'] = 'publish';
  return $args;
}
/* }}} */

/*{{{ ACF : Affiche la description du terme dans une infobulle au survol si elle existe */
add_filter( 'acf/fields/taxonomy/result', 'mwm_acf_term_description', 20, 4 );
function mwm_acf_term_description( $title, $term, $field, $post_id ) {
  if ( $term -> description ) {
    $title = '<span title = "' . $term -> description .  '">' . $title . '</span>';
  }
  return $title;
}
/*}}}*/

/*{{{ ACF : Peuple le champ déroulant « xxx » */
add_filter( 'acf/load_field/name=xxx', 'mwm_acf_populate_xxx' );
function mwm_acf_populate_xxx( $field ) {
  $field['choices'][ 'xxx' ] = "xxx";
  $args = array(
    'post_type' => 'xxx',
    'posts_per_page' => -1,
    'orderby' => 'title',
    'order' => 'ASC',
  );
  $query = new WP_Query( $args );
  if ( $query -> have_posts() ) {
    while ( $query -> have_posts() ) {
      $query -> the_post();
      $field['choices'][ $query -> post -> ID ] = $query -> post -> post_title;
    }
    wp_reset_postdata();
  }
  return $field;
}
/*}}}*/

/*{{{ Enfold : Charge des polices Google Fonts supplémentaires */
add_filter( 'avf_google_heading_font', 'mwm_avia_add_heading_font' );
function mwm_avia_add_heading_font( $fonts ){
  $fonts['Nunito'] = 'Nunito';
  return $fonts;
}
add_filter( 'avf_google_content_font', 'mwm_avia_add_content_font' );
function mwm_avia_add_content_font( $fonts ){
  $fonts['Nunito'] = 'Nunito';
  $fonts['Spectral'] = 'Spectral';
  return $fonts;
}
/*}}}*/

/*{{{ Enfold : Active ALB sur des types d'articles */
add_filter( 'avf_alb_supported_post_types', 'mwm_avia_add_alb_to_post_type', 10, 1 );
function mwm_avia_add_alb_to_post_type( $supported_post_types ) {
    $supported_post_types[] = 'xxx';
  }
  return $supported_post_types;
}
/*}}}*/

/*{{{ Enfold : Charge les shortcodes du thème enfant */
add_filter( 'avia_load_shortcodes', 'mwm_avia_include_shortcode_template', 15, 1 );
function mwm_avia_include_shortcode_template( $paths ){
  $template_url = get_stylesheet_directory();
  array_unshift( $paths, $template_url . '/shortcodes/' );
  return $paths;
}
/*}}}*/

/*{{{ Enfold : Désactive le Layerslider */
add_theme_support( 'deactivate_layerslider' );
/*}}}*/

/*{{{ Enfold : Désactive le Portfolio */
add_action( 'after_setup_theme' , 'mwm_enfold_remove_portfolio' );
function mwm_enfold_remove_portfolio() {
  remove_action( 'init', 'portfolio_register' );
}
/*}}}*/

/*{{{ Enfold: Désactive le bouton d'insertion de shortcodes Avia dans TinyMCE */
add_filter( 'mce_external_plugins', 'mwm_enfold_disable_tinymce_button', 999, 1 );
function mwm_enfold_disable_tinymce_button( $plugins ) {
  unset( $plugins['avia_builder_button'] );
  return $plugins;
}
/*}}}*/

/*{{{ Enfold: Supprime les tailles d'images par défaut */
add_filter( 'avf_modify_thumb_size', 'mwm_enfold_remove_image_sizes', 10, 1 );
function mwm_enfold_remove_image_sizes( $sizes ) {
  return null;
}
add_filter( 'init', 'mwm_enfold_remove_shop_image_sizes', 10, 1 );
function mwm_enfold_remove_shop_image_sizes( $sizes ) {
  remove_image_size( 'shop_thumbnail' );
  remove_image_size( 'shop_catalog' );
  remove_image_size( 'shop_single' );
  remove_image_size( 'medium_large' );
}
/*}}}*/

/*{{{ AJAX: Formulaire admin */
/*{{{ Create the HTML form */
ob_start();
?>
<form action = "<?php echo admin_url( 'admin-ajax.php' ); ?>" method = "post" id = "" enctype = "multipart/form-data" >
  <input type = "text" name = "" id = "" value = "" />
  <input type = "hidden" name = "action" value = "xxx" />
  <?php wp_nonce_field( 'xxx_nonce', 'xxx_nonce_field', true ); ?>
  <input type = "submit" class = "" value = "Valider" />
  <div class = "message" ></div>
</form>
<?php $form = ob_get_clean();
/*}}}*/
/*{{{ Process the form */
add_action( 'wp_ajax_xxx', 'mwm_xxx' );
function mwm_xxx() {
  /*{{{ Sécurité */
  if (
    !wp_verify_nonce( $_REQUEST['xxx_nonce_field'], 'xxx_nonce' )
  ) {
    error_log( print_r( "Invalid nonce", true ) );
    exit( 'Nonce is not valid' );
  }
  /*}}}*/
  $response = array( 'error' => false );
  $field = sanitize_text_field( $_POST['xxx'] );
  if ( $field == '' ) {
    $response['error'] = true;
    $response['error_message'] = "empty field";
    exit( json_encode( $response) );
  } else {
    $response['success_message'] = "Success";
    // Do more stuff
  }
  exit( json_encode( $response ) );
}
/*}}}*/
/*{{{ Javascript */
  $('#xxx-form').on('submit', function(e) {
    e.preventDefault();
    $('.spinner').show();
    var form = $(this);
    $.post(form.attr('action'), form.serialize(), function(data) {
      // console.log('ajax callback : ' + JSON.stringify(data));
      $('.spinner').hide();
      if(data.error){
        $('#xxx-form .message').html(data.error_message);
      } else {
        $('#xxx-form .message').html(data.success_message);
      }
    }, 'json');
  });
/*}}}*/
/*}}}*/

/*{{{ Redirige les utilisateurs après connexion selon leur role */
add_filter( 'login_redirect', 'mwm_login_redirect', 10, 3 );
function mwm_login_redirect( $redirect_to, $request, $user ) {
  global $user;
  if (
    isset( $user -> roles ) &&
    is_array( $user -> roles )
  ) {
    if (
      in_array( "xxx", $user -> roles )
    ) {
      return get_bloginfo( 'wpurl' ) . '/wp-admin/index.php';
    }
  } else {
    return $redirect_to;
  }
}
/*}}}*/

/*{{{ Supprime la modification rapide */
add_filter( 'page_row_actions', 'mwm_remove_quick_edit', 10, 1 );
add_filter( 'post_row_actions', 'mwm_remove_quick_edit', 10, 1 );
function mwm_remove_quick_edit( $actions ) {
  if ( isset( $actions['inline hide-if-no-js'] ) ) {
    $post_types_to_filter = array( 'xxx', 'xxx' );
    $current_post_type = $_GET['post_type'];
    if ( in_array( $current_post_type, $post_types_to_filter ) ) {
      unset( $actions['inline hide-if-no-js'] );
    }
  }
  return $actions;
}
/*}}}*/

/*{{{ Empêche les fonds noir des vignettes PDF auto-générées */
// https://core.trac.wordpress.org/ticket/45982
require_once ABSPATH . 'wp-includes/class-wp-image-editor.php';
require_once ABSPATH . 'wp-includes/class-wp-image-editor-imagick.php';
final class ExtendedWpImageEditorImagick extends WP_Image_Editor_Imagick {
  protected function pdf_load_source() {
    $loaded = parent::pdf_load_source();
    try {
      $this -> image -> setImageAlphaChannel( Imagick::ALPHACHANNEL_REMOVE );
      $this -> image -> setBackgroundColor( '#ffffff' );
    } catch ( Exception $exception ) {
      error_log( $exception -> getMessage() );
    }
    return $loaded;
  }
}
add_filter( 'wp_image_editors', function ( array $editors ) : array {
  array_unshift( $editors, ExtendedWpImageEditorImagick::class );
  return $editors;
} );
/*}}}*/

/*{{{ Ajoute les crédits MWM sur le pied de page */
add_filter( 'admin_footer_text', 'mwm_add_custom_admin_footer' );
function mwm_add_custom_admin_footer () {
  echo __( 'Developed by', 'mwm_add_custom_admin_footer' ) . ' <a href = "http://www.mwm-webdesign.com" title = "' . __( 'MWM Webdesign - Webdesign and custom Wordpress development', 'mwm_add_custom_admin_footer' ) . '" target = "_blank">' . __( 'MWM Webdesign', 'mwm_add_custom_admin_footer' ) . '</a> | ';
  echo __( 'Powered by', 'mwm_add_custom_admin_footer' ) . ' <a href = "http://www.wordpress.org" title = "' . __( 'Open source CMS', 'mwm_add_custom_admin_footer' ) . '" target = "_blank">WordPress</a>';
}
/*}}}*/

/*}}}*/

/*{{{ Login */

/*{{{ Charge les scripts de la page de login */
add_action( 'login_enqueue_scripts', 'mwm_csud_login_enqueue_scripts' );
function mwm_csud_login_enqueue_scripts() {
  wp_enqueue_script(
    'xxx_login',
    get_stylesheet_directory_uri() . '/js/login.js',
    array( 'jquery' )
  );
}
/*}}}*/

/*{{{ Charge les styles de la page de login */
add_action( 'login_enqueue_scripts', 'mwm_load_login_styles' );
function mwm_load_login_styles() {
  wp_enqueue_style(
    'xxx_login',
    get_stylesheet_directory_uri() . '/css/login.css'
  );
}
/*}}}*/

/*{{{ Modifie l'URL et le titre du logo de la page de login */
add_filter( 'login_headerurl', 'mwm_login_logo_url' );
function mwm_login_logo_url() {
  return home_url();
}
add_filter( 'login_headertitle', 'mwm_login_logo_link_title' );
function mwm_login_logo_link_title() {
  return 'xxx';
}
/*}}}*/

/*}}}*/

/*{{{ Helpers */

/*{{{ mwm_acf_list_terms() : Retourne la liste des termes attachés à un article depuis un champ taxonomie ACF  */
function mwm_acf_list_terms( $id, $key, $separator = ', ', $before = '', $after = '', $link = 0, $lang = '', $use_wp = false ) {
  if ( !function_exists( 'get_field' ) ) {
    return false;
  }
  if ( $use_wp ) {
    $terms = get_the_terms( $id, $key );
  } else {
    $terms = get_field( $key, $id );
  }
  /* Traite les cas où seul un ID est enregistré */
  if ( is_numeric( reset( $terms ) ) ) {
    $terms_tmp = array();
    foreach ( $terms as $term_id ) {
      $term_o = get_term( $term_id );
      $terms_tmp[] = $term_o;
    }
    $terms = $terms_tmp;
  }
  if ( !is_array( $terms ) || empty( $terms ) ) {
    return false;
  }
  if ( is_wp_error( $terms ) ) {
    return $terms;
  }
  $output = '';
  if ( !empty( $before ) ) {
    $output .= $before;
  }
  /* Supprime les duplicatas (possibles sur termes hiérarchiques comme les localisations) */
  $terms = array_map( "unserialize", array_unique( array_map( "serialize", $terms ) ) );
  foreach ( $terms as $term ) {
    if ( $lang ) {
      $o_term_id = apply_filters( 'wpml_object_id', $term -> term_id, $term -> taxonomy, FALSE, $lang );
      $term = get_term_by( 'id', $o_term_id, $term -> taxonomy );
    }
    $term_name = $term -> name;
    if ( $link ) {
      $term_slug = $term -> slug;
      if ( empty( $taxonomy ) ) {
        $taxonomy = get_taxonomy( $term -> taxonomy );
        $taxonomy_rewrite = $taxonomy -> rewrite;
        $site_url = get_bloginfo( 'wpurl' );
      }
      $link_before = '<a href = "' . $site_url . '/' . $taxonomy_rewrite['slug'] . '/' . $term_slug . '" title = "' . $term_name . '" >';
      $link_after = '</a>';
      $output .= $link_before . $term_name . $link_after . $separator;
    } else {
      $output .= $term_name . $separator;
    }
  }
  $output = rtrim( $output, $separator );
  if ( !empty( $after ) ) {
    $output .= $after;
  }
  return $output;
}
/*}}}*/

/*{{{ mwm_message(): Retourne un message (info, erreur) */
function mwm_message( $message, $type ) {
  if ( !$message || !$type ) {
    return;
  }
  $output = '<div class = "message ' . $type . '" >';
  $output .= '<p>' . $message . '</p>';
  $output .= '</div>';
  return $output;
}
/*}}}*/

/*{{{ mwm_truncate_text() : Tronque un texte après un nombre de caractère défini */
function mwm_truncate_text( $text, $limit = 80, $ellipsis = '&hellip;' ) {
  if ( strlen( $text ) > $limit ) {
    $endpos = strpos( str_replace( array("\r\n", "\r", "\n", "\t"), ' ', $text ), ' ', $limit );
    if ( $endpos !== FALSE ) {
      $text = trim( substr( $text, 0, $endpos ) ) . $ellipsis;
    }
  }
  return $text;
}
/*}}}*/



/*}}}*/

/*{{{ Sécurité */

/*{{{ Rend le type de post articles par défaut inaccessible */
add_filter( 'register_post_type_args', 'mwm_remove_default_post_type', 0, 2 );
function mwm_remove_default_post_type( $args, $post_type ) {
  if ( $post_type === 'post' ) {
    $args['public']                = false;
    $args['show_ui']               = false;
    $args['show_in_menu']          = false;
    $args['show_in_admin_bar']     = false;
    $args['show_in_nav_menus']     = false;
    $args['can_export']            = false;
    $args['has_archive']           = false;
    $args['exclude_from_search']   = true;
    $args['publicly_queryable']    = false;
    $args['show_in_rest']          = false;
  }

  return $args;
}
/*}}}*/

/*}}}*/

/*{{{ Export */

/*{{{ Export CSV basique */
$args = array(
  'post_type' => array(
    'p_actualites',
    'p_communiques_presse',
    'p_documents_res'
  ),
  'status' => 'publish',
  'posts_per_page' => -1,
  'author__in' => mwm_get_csud_authors(),
  'year' => 2020
);
$query = new WP_Query( $args );
$sep = '@@@';
if ( $query -> have_posts() ) {
  while ( $query -> have_posts() ) {
    $query -> the_post();
    $titre = $query -> post -> post_title;
    $type = get_post_type_object( $query -> post -> post_type );
    $type = $type -> label;
    $surtitre = get_field ( 'sur-titre', $query -> post -> ID );
    $date = get_the_date( 'd/m/Y', $query -> post -> ID );
    $themes = mwm_acf_list_terms( $query -> post -> ID, 'themes' );
    $url = get_permalink( $query -> post -> ID );
    $extrait = get_field ( 'extrait', $query -> post -> ID );
    $data .= $titre . $sep . $surtitre . $sep . $type . $sep . $url . $sep . $themes . $sep . $extrait . $sep . $date . "\n";
  }
  file_put_contents('export.csv', $data);
  wp_reset_postdata();
} else {
  _e( 'no results', '' );
}
/*}}}*/

/*}}}*/

/*{{{ Debug */

/*{{{ Enfold : Mode Debug */
// add_action( 'avia_builder_mode', 'mwm_enfold_debug_mode' );
function mwm_enfold_debug_mode() {
  return "debug";
}
/*}}}*/

/*{{{ Liste les scripts chargés */
// add_action( 'wp_print_scripts', 'mwm_list_scripts' );
function mwm_list_scripts() {
  global $wp_scripts;
  echo '<pre>' . print_r( $wp_scripts -> queue, true ) . '</pre>';
}
/*}}}*/

/*{{{ Liste les styles chargés */
// add_action( 'wp_print_styles', 'mwm_list_styles' );
function mwm_list_styles() {
  global $wp_styles;
  echo '<pre>' . print_r( $wp_styles -> queue, true ) . '</pre>';
}
/*}}}*/

/*}}}*/

/*{{{ Optimisations */

/*{{{ Annule le chargement de certains scripts */
// add_action( 'wp_enqueue_scripts', 'mwm_dequeue_scripts', 10000 );
function mwm_dequeue_scripts() {
  wp_dequeue_script( 'wptoolset-field-date' );
}
/*}}}*/

/*{{{ Annule le chargement de certains styles */
// add_action( 'wp_enqueue_styles', 'mwm_dequeue_styles', 10000 );
function mwm_dequeue_styles() {
  wp_dequeue_style( 'wptoolset-field-datepicker' );
}
/*}}}*/

/*{{{ Supprime les nags des plugins */
add_filter( 'remove_hube2_nag', '__return_true' );
/*}}}*/

/*}}}*/
